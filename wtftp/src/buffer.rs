// Courtesy of Globi
// https://github.com/Globidev/twitch-player/blob/master/twitchd/src/prelude/stream_ext.rs#L108-L160
// Modified to read exact amounts

use futures::{Async, Poll, Stream};

pub struct BufferConcat<S>
where
    S: Stream,
    S::Item: AsRef<[u8]>,
{
    stream: S,
    buf_size: usize,
    buffered: Vec<u8>,
    spilled: Vec<u8>,
    err: Option<S::Error>,
}

impl<S> BufferConcat<S>
where
    S: Stream,
    S::Item: AsRef<[u8]>,
{
    pub fn new(stream: S, buf_size: usize) -> BufferConcat<S> {
        let buffered = Vec::with_capacity(buf_size);
        let spilled = Vec::with_capacity(buf_size * 4); // not sure what acceptable value to put here

        Self {
            stream,
            buf_size,
            buffered,
            spilled,
            err: None,
        }
    }

    fn take_buffer(&mut self) -> Vec<u8> {
        std::mem::replace(&mut self.buffered, Vec::with_capacity(self.buf_size))
    }
}

impl<S> Stream for BufferConcat<S>
where
    S: Stream,
    S::Item: AsRef<[u8]>,
{
    type Item = Vec<u8>;
    type Error = S::Error;

    fn poll(&mut self) -> Poll<Option<Self::Item>, Self::Error> {
        // If data spilled during the last poll, transfer what we can back onto the buffer
        if !self.spilled.is_empty() {
            let bufferable_len = std::cmp::min(self.buf_size, self.spilled.len());
            let remaining = self.spilled.split_off(bufferable_len);
            let bufferable = std::mem::replace(&mut self.spilled, remaining);
            self.buffered.extend_from_slice(&bufferable);

            // Spilled transfer might have filled the buffer again, yield it if that's the case
            if bufferable_len >= self.buf_size {
                return Ok(Async::Ready(Some(self.take_buffer())));
            }
        }

        // Abort right away if we encountered an error during the last poll
        if let Some(err) = self.err.take() {
            return Err(err);
        }
        // Here we are gonna poll the inner stream in a loop because we might
        // be able to buffer multiple consecutive ready items
        loop {
            match self.stream.poll() {
                // If the inner stream is not ready then we wait
                Ok(Async::NotReady) => return Ok(Async::NotReady),
                // In case the inner stream errored
                Err(error) => {
                    return match self.buffered.is_empty() {
                        // If the buffer is empty we can abort with the error right
                        // away
                        true => Err(error),
                        // If we still have buffered items, we first need to yield
                        // them back before erroring at the next call to poll
                        false => {
                            self.err = Some(error);
                            Ok(Async::Ready(Some(self.take_buffer())))
                        }
                    };
                }
                // If the inner stream has some item ready, we can buffer it and
                // maybe yield the buffer back if we reached the capacity
                Ok(Async::Ready(Some(data))) => {
                    let bytes = data.as_ref();

                    let max_bufferable_len = self.buf_size - self.buffered.len();
                    let bufferable_len = std::cmp::min(max_bufferable_len, bytes.len());
                    let (bufferable, spilled) = bytes.split_at(bufferable_len);

                    self.buffered.extend_from_slice(bufferable);
                    self.spilled.extend_from_slice(spilled);

                    if self.buffered.len() >= self.buf_size {
                        return Ok(Async::Ready(Some(self.take_buffer())));
                    }
                }
                // In case the stream ended
                Ok(Async::Ready(None)) => {
                    return match self.buffered.is_empty() {
                        // If the buffer is empty, we can end right away
                        true => Ok(Async::Ready(None)),
                        // Otherwise we need to yield our buffer before ending at
                        // the next call to poll
                        false => Ok(Async::Ready(Some(self.take_buffer()))),
                    };
                }
            }
        }
    }
}
