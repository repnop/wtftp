#![feature(async_await, await_macro, futures_api)]

use libwtftp::{Ack, Data, HostMode, TftpConnection, TftpListener, TftpPacket};
use std::{
    convert::TryFrom,
    io::{ErrorKind, Read},
};

fn handle_connection(mut conn: TftpConnection) {
    match conn.host_mode {
        HostMode::Reading => {
            let mut file_buffer = Vec::new();
            let mut packet_buffer = vec![0; 516];
            std::fs::File::open(std::str::from_utf8(conn.requested_file.as_bytes()).unwrap())
                .unwrap()
                .read_to_end(&mut file_buffer)
                .unwrap();
            conn.socket
                .set_read_timeout(Some(std::time::Duration::from_millis(500)))
                .unwrap();

            for chunk in file_buffer.chunks(512) {
                let packet = TftpPacket::Data(Data {
                    block_number: conn.block_number,
                    data: chunk.to_vec(),
                });

                let encoded = Vec::from(&packet);

                'inner: loop {
                    conn.socket.send_to(&encoded, conn.send_to).unwrap();

                    let len = match conn.socket.recv(&mut packet_buffer) {
                        Ok(len) => len,
                        Err(ref e) if e.kind() == ErrorKind::TimedOut => continue 'inner,
                        Err(_) => return,
                    };

                    if let TftpPacket::Ack(Ack { block_number }) =
                        TftpPacket::try_from(&packet_buffer[..len]).unwrap()
                    {
                        if block_number == conn.block_number {
                            conn.block_number += 1;
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }

                    break 'inner;
                }
            }
        }
        HostMode::Writing => {}
    }
}

fn main() {
    for conn in TftpListener::new([127, 0, 0, 1], 69).unwrap().incoming() {
        println!("{:?}", conn);
        std::thread::spawn(move || {
            handle_connection(conn);
        });
    }
}
