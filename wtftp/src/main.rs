mod buffer;

use futures::Stream;
use libwtftp::{
    asynchronous::{AsyncTftpConnection, AsyncTftpListener},
    Ack, Data, Error, HostMode, TftpError, TftpErrorCode, TftpPacket,
};
use log::{error, info};
use std::{
    convert::TryInto,
    time::{Duration, Instant},
};
#[cfg(any(target_os = "linux", target_os = "unix"))]
use structopt::StructOpt;
use tokio::{
    codec::{BytesCodec, Decoder},
    net::UdpSocket,
    prelude::{
        future::{Either, Loop},
        *,
    },
};

#[cfg(any(target_os = "linux", target_os = "unix"))]
#[derive(Debug, StructOpt)]
struct Options {
    #[structopt(subcommand)]
    command: Command,
}

#[cfg(any(target_os = "linux", target_os = "unix"))]
#[derive(Debug, StructOpt)]
enum Command {
    #[structopt(name = "start")]
    Start,
    #[structopt(name = "stop")]
    Stop,
    #[structopt(name = "restart")]
    Restart,
}

fn retry_recv_timeout(
    sock: UdpSocket,
    max_retries: usize,
    timeout: Duration,
) -> impl Future<Item = (UdpSocket, Vec<u8>, usize, std::net::SocketAddr), Error = TftpError> {
    future::loop_fn((1, sock), move |(attempt, sock)| {
        let resp = vec![0; 516];

        let recv_with_timeout = sock
            .recv_dgram(resp)
            .select2(tokio::timer::Delay::new(Instant::now() + timeout));

        recv_with_timeout.then(move |result| match result {
            Ok(Either::A((dgram, _delay))) => Ok(Loop::Break(dgram)),
            Ok(Either::B((_timed_out, recv))) => {
                let sock = recv.into_parts().socket;
                if attempt >= max_retries {
                    Err(TftpError::Timeout)
                } else {
                    Ok(Loop::Continue((attempt + 1, sock)))
                }
            }
            Err(Either::A((io_error, _delay))) => Err(TftpError::IoError(io_error)),
            Err(Either::B((timer_error, _recv))) => Err({
                error!("Timer error: {:?}", timer_error);
                TftpError::Other
            }),
        })
    })
}

fn send_packet(
    packet: Vec<u8>,
    sock: UdpSocket,
    mut conn: AsyncTftpConnection,
) -> impl Future<Item = (UdpSocket, AsyncTftpConnection), Error = TftpError> {
    sock.send_dgram(packet, &conn.send_to.into())
        .map_err(Into::into)
        .and_then(|(sock, _addr)| {
            retry_recv_timeout(sock, 6, Duration::from_millis(250)).and_then(
                |(sock, resp_bytes, len, _)| {
                    match (&resp_bytes[..len]).try_into() {
                        Ok(TftpPacket::Ack(Ack { block_number }))
                            if block_number == conn.block_number =>
                        {
                            conn.block_number += 1
                        }
                        _ => {
                            send_packet(
                                Vec::from(&TftpPacket::<Vec<u8>>::Error(Error {
                                    error_code: TftpErrorCode::NotDefined,
                                    error_message: (&[][..]).try_into().unwrap(),
                                })),
                                sock,
                                conn,
                            );
                            return Err(TftpError::TftpErrorCode(TftpErrorCode::NotDefined));
                        }
                    }

                    Ok((sock, conn))
                },
            )
        })
}

fn handle_connection(
    (sock, conn): (UdpSocket, AsyncTftpConnection),
) -> impl Future<Item = (), Error = TftpError> {
    use std::io::ErrorKind;
    info!("Got client: {}", conn.send_to.0);
    let err_sock = UdpSocket::bind(&(sock.local_addr().unwrap().ip(), 0).into()).unwrap();
    let err_conn = conn.clone();
    let host_mode = conn.host_mode;
    match host_mode {
        HostMode::Reading => tokio::fs::File::open(conn.requested_file.as_str().to_string())
            .map_err(|e| {
                let err = match e.kind() {
                    ErrorKind::NotFound => TftpErrorCode::FileNotFound,
                    ErrorKind::PermissionDenied => TftpErrorCode::AccessViolation,
                    _ => TftpErrorCode::NotDefined,
                };

                let _ = send_packet(
                    Vec::from(&TftpPacket::<Vec<u8>>::Error(Error {
                        error_code: err,
                        error_message: (&[][..]).try_into().unwrap(),
                    })),
                    err_sock,
                    err_conn,
                )
                .wait();

                e.into()
            })
            .and_then(|mut file| {
                let metadata = file.poll_metadata().unwrap();
                Ok((file, metadata))
            })
            .and_then(move |(file, metadata)| {
                let metadata = if let Async::Ready(metadata) = metadata {
                    metadata
                } else {
                    unreachable!()
                };

                info!("Reading {}", conn.requested_file.as_str());

                let file_len = metadata.len();
                let needs_empty_packet = file_len % 512 == 0;

                let inst = Instant::now();

                buffer::BufferConcat::new(BytesCodec::new().framed(file), 512)
                    .map_err(|_| TftpError::Other)
                    .fold((sock, conn), move |(sock, conn), chunk| {
                        let data = chunk;
                        let packet = TftpPacket::Data(Data {
                            block_number: conn.block_number,
                            data,
                        });

                        let packet_data = Vec::from(&packet);

                        send_packet(packet_data, sock, conn)
                    })
                    .and_then(move |(sock, conn)| {
                        if needs_empty_packet {
                            let empty_packet = Vec::from(&TftpPacket::Data(Data {
                                block_number: conn.block_number,
                                data: vec![],
                            }));
                            future::Either::A(send_packet(empty_packet, sock, conn).map(|_| {}))
                        } else {
                            future::Either::B(future::ok(()))
                        }
                    })
                    .map(move |_| {
                        let time = Instant::now().duration_since(inst).as_secs();
                        info!(
                            "Transfer successful: {} bytes transferred in {} seconds ({} B/s)",
                            file_len,
                            time,
                            file_len / time,
                        )
                    })
            }),
        HostMode::Writing => unimplemented!(),
    }
}

fn main() {
    env_logger::init();

    #[cfg(any(target_os = "linux", target_os = "unix"))]
    {
        const PID: &str = "/var/run/wtftp.pid";
        let options = Options::from_args();

        match options.command {
            Command::Start => {}
            Command::Stop => match std::fs::read_to_string(PID) {
                Ok(s) => {
                    let pid = nix::unistd::Pid::from_raw(s.trim().parse::<i32>().unwrap());

                    if let Err(e) = nix::sys::signal::kill(pid, nix::sys::signal::Signal::SIGKILL) {
                        eprintln!("Error killing process {}: {:?}", pid.as_raw(), e);
                    }

                    std::process::exit(1);
                }
                Err(e) => {
                    eprintln!("Error reading .pid file: {:?}", e);
                    std::process::exit(1);
                }
            },
            Command::Restart => match std::fs::read_to_string(PID) {
                Ok(s) => {
                    let pid = nix::unistd::Pid::from_raw(s.trim().parse::<i32>().unwrap());
                    if let Err(e) = nix::sys::signal::kill(pid, nix::sys::signal::Signal::SIGKILL) {
                        eprintln!("Error killing process {}: {:?}", pid.as_raw(), e);
                        std::process::exit(1);
                    }

                    std::thread::sleep(Duration::from_millis(250));
                }
                Err(ref e) if e.kind() == std::io::ErrorKind::NotFound => {}
                Err(e) => {
                    eprintln!("Error reading .pid file: {:?}", e);
                    std::process::exit(1);
                }
            },
        }

        let mut log_file = std::fs::OpenOptions::new()
            .create(true)
            .append(true)
            .open("/var/log/wtftp.log")
            .unwrap();

        let daemonizer = daemonize::Daemonize::new()
            .pid_file(PID)
            .stdout(log_file.try_clone().unwrap())
            .stderr(log_file.try_clone().unwrap());

        match daemonizer.start() {
            Ok(_) => info!("Daemonized successfully"),
            Err(e) => {
                writeln!(log_file, "[ERROR] Error while daemonizing: {:?}", e).unwrap();
                std::process::exit(1);
            }
        }
    }

    tokio::run(
        AsyncTftpListener::new([0, 0, 0, 0], 69)
            .unwrap()
            .then(|res| {
                if let Ok(fut) = res {
                    tokio::spawn(handle_connection(fut).map_err(|e| error!("{:?}", e)));
                } else {
                    error!("{:?}", res);
                }

                Ok(())
            })
            .for_each(|_| Ok(())),
    );
}
