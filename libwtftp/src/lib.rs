#[cfg(feature = "async")]
pub mod asynchronous;
mod netascii;

use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};
use netascii::NetAsciiString;
use std::{
    convert::{TryFrom, TryInto},
    io::Write,
    net::{IpAddr, UdpSocket},
};

fn read_bytes_while<'a, F: Fn(&u8) -> bool>(bytes: &mut &'a [u8], f: F) -> &'a [u8] {
    let end = bytes.iter().take_while(|b| f(b)).count();

    let slice = &bytes[..end];
    *bytes = &bytes[end + 1..];

    slice
}

#[derive(Debug, Clone)]
pub enum TftpPacket<T: AsRef<[u8]> = Vec<u8>> {
    ReadRequest(ReadRequest),
    WriteRequest(WriteRequest),
    Data(Data<T>),
    Ack(Ack),
    Error(Error),
}

impl TryFrom<&'_ [u8]> for TftpPacket<Vec<u8>> {
    type Error = TftpError;

    fn try_from(bytes: &[u8]) -> Result<Self, TftpError> {
        let bytes = &mut &bytes[..];
        let opcode: TftpOpcode = bytes.read_u16::<BigEndian>()?.try_into()?;

        Ok(match opcode {
            TftpOpcode::ReadRequest => TftpPacket::ReadRequest(ReadRequest {
                filename: NetAsciiString::try_from(read_bytes_while(bytes, |b| *b != 0))?,
                mode: std::str::from_utf8(read_bytes_while(bytes, |b| *b != 0))
                    .unwrap()
                    .try_into()?,
            }),
            TftpOpcode::WriteRequest => TftpPacket::WriteRequest(WriteRequest {
                filename: NetAsciiString::try_from(read_bytes_while(bytes, |b| *b != 0))?,
                mode: std::str::from_utf8(read_bytes_while(bytes, |b| *b != 0))
                    .unwrap()
                    .try_into()?,
            }),
            TftpOpcode::Data => TftpPacket::Data(Data {
                block_number: bytes.read_u16::<BigEndian>()?,
                data: (&bytes[..]).to_vec(),
            }),
            TftpOpcode::Acknowledgement => TftpPacket::Ack(Ack {
                block_number: bytes.read_u16::<BigEndian>()?,
            }),
            TftpOpcode::Error => TftpPacket::Error(Error {
                error_code: TftpErrorCode::try_from(bytes.read_u16::<BigEndian>()?)?,
                error_message: NetAsciiString::try_from(read_bytes_while(bytes, |b| *b != 0))?,
            }),
        })
    }
}

impl<T: AsRef<[u8]>> From<&'_ TftpPacket<T>> for Vec<u8> {
    fn from(packet: &TftpPacket<T>) -> Vec<u8> {
        let mut buffer = Vec::with_capacity(516);

        match packet {
            TftpPacket::ReadRequest(rrq) => {
                buffer
                    .write_u16::<BigEndian>(TftpOpcode::ReadRequest as u16)
                    .unwrap();
                buffer.write_all(rrq.filename.as_bytes()).unwrap();
                buffer.write_u8(0).unwrap();
                buffer.write_all(rrq.mode.as_str().as_bytes()).unwrap();
            }
            TftpPacket::WriteRequest(rrq) => {
                buffer
                    .write_u16::<BigEndian>(TftpOpcode::WriteRequest as u16)
                    .unwrap();
                buffer.write_all(rrq.filename.as_bytes()).unwrap();
                buffer.write_u8(0).unwrap();
                buffer.write_all(rrq.mode.as_str().as_bytes()).unwrap();
            }
            TftpPacket::Data(data) => {
                buffer
                    .write_u16::<BigEndian>(TftpOpcode::Data as u16)
                    .unwrap();
                buffer.write_u16::<BigEndian>(data.block_number).unwrap();
                buffer.write_all(data.data.as_ref()).unwrap();
            }
            TftpPacket::Ack(ack) => {
                buffer
                    .write_u16::<BigEndian>(TftpOpcode::Acknowledgement as u16)
                    .unwrap();
                buffer.write_u16::<BigEndian>(ack.block_number).unwrap();
            }
            TftpPacket::Error(error) => {
                buffer
                    .write_u16::<BigEndian>(TftpOpcode::Error as u16)
                    .unwrap();
                buffer
                    .write_u16::<BigEndian>(error.error_code as u16)
                    .unwrap();
                buffer.write_all(error.error_message.as_bytes()).unwrap();
                buffer.write_u8(0).unwrap();
            }
        }

        buffer
    }
}

#[derive(Debug, Clone)]
pub struct ReadRequest {
    pub filename: NetAsciiString,
    pub mode: TftpMode,
}

#[derive(Debug, Clone)]
pub struct WriteRequest {
    pub filename: NetAsciiString,
    pub mode: TftpMode,
}

#[derive(Debug, Clone)]
pub struct Data<T: AsRef<[u8]>> {
    pub block_number: u16,
    pub data: T,
}

#[derive(Debug, Clone)]
pub struct Ack {
    pub block_number: u16,
}

#[derive(Debug, Clone)]
pub struct Error {
    pub error_code: TftpErrorCode,
    pub error_message: NetAsciiString,
}

#[derive(Debug, Clone, Copy)]
#[repr(u16)]
pub enum TftpOpcode {
    /// Request to read data from a file.
    ReadRequest = 1,
    /// Request to write to a file.
    WriteRequest = 2,
    /// Data packet, used in read requests.
    Data = 3,
    /// Acknowledgement packet used to signify the last packet was received
    /// (essentially TCP-lite).
    Acknowledgement = 4,
    /// Error packet.
    Error = 5,
}

impl TryFrom<u16> for TftpOpcode {
    type Error = TftpError;

    fn try_from(opcode: u16) -> Result<TftpOpcode, TftpError> {
        Ok(match opcode {
            1 => TftpOpcode::ReadRequest,
            2 => TftpOpcode::WriteRequest,
            3 => TftpOpcode::Data,
            4 => TftpOpcode::Acknowledgement,
            5 => TftpOpcode::Error,
            _ => return Err(TftpError::TftpErrorCode(TftpErrorCode::IllegalOperation)),
        })
    }
}

#[derive(Debug, Clone, Copy)]
pub enum TftpMode {
    /// Netascii (text) mode, uses the netascii text specification which is an
    /// 8-bit ASCII-based encoding.
    Netascii,
    /// Raw binary transfer mode (previously called `binary`) in which
    /// individual octets are sent, up to 512 at a time.
    Octet,
    /// Mail mode is deprecated and is not handled, but left here for
    /// completion's sake.
    #[allow(unused)]
    Mail,
}

impl TftpMode {
    pub fn as_str(self) -> &'static str {
        match self {
            TftpMode::Netascii => "netascii",
            TftpMode::Octet => "octet",
            TftpMode::Mail => panic!("Mail mode is deprecated and not handled"),
        }
    }
}

impl TryFrom<&'_ str> for TftpMode {
    type Error = TftpModeError;

    fn try_from(s: &str) -> Result<TftpMode, TftpModeError> {
        if s.eq_ignore_ascii_case("NETASCII") {
            Ok(TftpMode::Netascii)
        } else if s.eq_ignore_ascii_case("OCTET") {
            Ok(TftpMode::Octet)
        } else if s.eq_ignore_ascii_case("MAIL") {
            Err(TftpModeError::MailRequest)
        } else {
            Err(TftpModeError::Other(s.into()))
        }
    }
}

#[derive(Debug, Clone)]
pub enum TftpModeError {
    MailRequest,
    Other(String),
}

#[derive(Debug, Clone, Copy)]
#[repr(u16)]
pub enum TftpErrorCode {
    /// Not defined, see error message.
    NotDefined = 0,
    FileNotFound = 1,
    AccessViolation = 2,
    DiskFull = 3,
    IllegalOperation = 4,
    UnknownTransferId = 5,
    FileAlreadyExists = 6,
    NoSuchUser = 7,
}

impl TryFrom<u16> for TftpErrorCode {
    type Error = TftpError;

    fn try_from(opcode: u16) -> Result<TftpErrorCode, TftpError> {
        Ok(match opcode {
            0 => TftpErrorCode::NotDefined,
            1 => TftpErrorCode::FileNotFound,
            2 => TftpErrorCode::AccessViolation,
            3 => TftpErrorCode::DiskFull,
            4 => TftpErrorCode::IllegalOperation,
            5 => TftpErrorCode::UnknownTransferId,
            6 => TftpErrorCode::FileAlreadyExists,
            7 => TftpErrorCode::NoSuchUser,
            i => return Err(TftpError::UnknownErrorCode(i)),
        })
    }
}

#[derive(Debug)]
pub enum TftpError {
    NetAsciiError(netascii::NetAsciiError),
    IoError(std::io::Error),
    TftpErrorCode(TftpErrorCode),
    UnknownErrorCode(u16),
    TftpModeError(TftpModeError),
    Timeout,
    Other,
}

impl From<netascii::NetAsciiError> for TftpError {
    fn from(e: netascii::NetAsciiError) -> TftpError {
        TftpError::NetAsciiError(e)
    }
}

impl From<std::io::Error> for TftpError {
    fn from(e: std::io::Error) -> Self {
        TftpError::IoError(e)
    }
}

impl From<TftpErrorCode> for TftpError {
    fn from(e: TftpErrorCode) -> Self {
        TftpError::TftpErrorCode(e)
    }
}

impl From<TftpModeError> for TftpError {
    fn from(e: TftpModeError) -> Self {
        TftpError::TftpModeError(e)
    }
}

#[derive(Debug, Clone, Copy)]
pub enum HostMode {
    Reading,
    Writing,
}

pub struct TftpListener {
    addr: IpAddr,
    socket: UdpSocket,
}

impl TftpListener {
    pub fn new<I: Into<IpAddr>>(addr: I, port: u16) -> std::io::Result<Self> {
        let addr = addr.into();
        Ok(Self {
            addr,
            socket: UdpSocket::bind((addr, port))?,
        })
    }

    pub fn accept(&self) -> TftpConnection {
        loop {
            let data = &mut [0u8; 516];
            let (len, sock) = match self.socket.recv_from(data) {
                Ok((len, sock)) => (len, sock),
                Err(e) => {
                    eprintln!("{:?}", e);
                    continue;
                }
            };

            let packet = match TftpPacket::try_from(&data[..len]) {
                Ok(pkt) => pkt,
                Err(e) => {
                    eprintln!("{:?}", e);
                    continue;
                }
            };

            let socket = match UdpSocket::bind((self.addr, 0)) {
                Ok(sock) => sock,
                Err(e) => {
                    eprintln!("{:?}", e);
                    continue;
                }
            };

            socket.connect((sock.ip(), sock.port())).unwrap();

            let (mode, host_mode, requested_file) = match packet {
                TftpPacket::ReadRequest(rrq) => (rrq.mode, HostMode::Reading, rrq.filename),

                TftpPacket::WriteRequest(_) => unimplemented!(),
                _ => continue,
            };

            return TftpConnection {
                socket,
                block_number: 1,
                mode,
                host_mode,
                requested_file,
            };
        }
    }

    pub fn incoming(&self) -> Incoming {
        Incoming { listener: self }
    }
}

pub struct Incoming<'a> {
    listener: &'a TftpListener,
}

impl<'a> Iterator for Incoming<'a> {
    type Item = TftpConnection;

    fn next(&mut self) -> Option<TftpConnection> {
        Some(self.listener.accept())
    }
}

#[derive(Debug)]
pub struct TftpConnection {
    pub socket: UdpSocket,
    pub block_number: u16,
    pub mode: TftpMode,
    pub host_mode: HostMode,
    pub requested_file: NetAsciiString,
}
