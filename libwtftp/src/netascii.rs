use std::convert::TryFrom;

#[derive(Debug, Clone)]
pub struct NetAsciiString(String);

impl NetAsciiString {
    pub fn as_bytes(&self) -> &[u8] {
        self.0.as_bytes()
    }

    pub fn as_str(&self) -> &str {
        std::str::from_utf8(self.as_bytes()).unwrap()
    }
}

impl TryFrom<&'_ [u8]> for NetAsciiString {
    type Error = NetAsciiError;

    // TODO: replace LF with CRLF and CR with CR NUL
    fn try_from(bytes: &[u8]) -> Result<NetAsciiString, NetAsciiError> {
        bytes
            .iter()
            .cloned()
            .map(netascii_filter)
            .collect::<Result<String, NetAsciiError>>()
            .map(NetAsciiString)
    }
}

fn netascii_filter(byte: u8) -> Result<char, NetAsciiError> {
    match byte {
        0x00 | 0x0A | 0x0D => Ok(byte as char),
        0x20..=0x7F => Ok(byte as char),
        _ => Err(NetAsciiError::InvalidByte(byte)),
    }
}

#[derive(Debug)]
pub enum NetAsciiError {
    InvalidByte(u8),
}
