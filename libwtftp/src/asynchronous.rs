use crate::{HostMode, NetAsciiString, TftpError, TftpMode, TftpPacket};
use futures::{try_ready, Async, Poll, Stream};
use std::{convert::TryFrom, net::IpAddr};
use tokio::net::UdpSocket;

#[derive(Debug)]
pub struct AsyncTftpListener {
    addr: IpAddr,
    socket: UdpSocket,
}

impl AsyncTftpListener {
    pub fn new<I: Into<IpAddr>>(addr: I, port: u16) -> std::io::Result<Self> {
        let addr = addr.into();
        Ok(Self {
            addr,
            socket: UdpSocket::bind(&(addr, port).into())?,
        })
    }
}

#[derive(Debug)]
pub enum ListenerError {
    IoError(tokio::io::Error),
    TftpError(TftpError),
}

impl From<tokio::io::Error> for ListenerError {
    fn from(e: tokio::io::Error) -> ListenerError {
        ListenerError::IoError(e)
    }
}

impl From<TftpError> for ListenerError {
    fn from(e: TftpError) -> ListenerError {
        ListenerError::TftpError(e)
    }
}

impl Stream for AsyncTftpListener {
    type Item = (UdpSocket, AsyncTftpConnection);
    type Error = ListenerError;

    fn poll(&mut self) -> Poll<Option<(UdpSocket, AsyncTftpConnection)>, ListenerError> {
        let mut data = [0; 516];
        let (len, client) = try_ready!(self.socket.poll_recv_from(&mut data));

        let packet = TftpPacket::try_from(&data[..len])?;
        let socket = UdpSocket::bind(&(self.addr, 0).into())?;

        socket.connect(&client)?;

        let (mode, host_mode, requested_file) = match packet {
            TftpPacket::ReadRequest(rrq) => (rrq.mode, HostMode::Reading, rrq.filename),

            TftpPacket::WriteRequest(_) => unimplemented!(),
            _ => unimplemented!(),
        };

        let local_addr = client;

        Ok(Async::Ready(Some((
            socket,
            AsyncTftpConnection {
                send_to: (local_addr.ip(), local_addr.port()),
                block_number: 1,
                mode,
                host_mode,
                requested_file,
            },
        ))))
    }
}

#[derive(Debug, Clone)]
pub struct AsyncTftpConnection {
    pub send_to: (IpAddr, u16),
    pub block_number: u16,
    pub mode: TftpMode,
    pub host_mode: HostMode,
    pub requested_file: NetAsciiString,
}
